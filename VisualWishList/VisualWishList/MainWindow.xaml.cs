﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using VisualWishList.Models;
using VisualWishList.Services;

namespace VisualWishList
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        UserService userService;

        public MainWindow()
        {
            userService = UserService.getInstance();
            InitializeComponent();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Enter_Click(object sender, RoutedEventArgs e)
        {
            bool currentUserSet = userService.setCurrentUser(UserName.Text, UserPassword.Text);
            if(currentUserSet)
            {
                //Go to next page
                MessageBoxResult result = MessageBox.Show("Welcome " + UserName.Text, "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private User findUser(String name, String password)
        {
            User user = null;

            return user;
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
