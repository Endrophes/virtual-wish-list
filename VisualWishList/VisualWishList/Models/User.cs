﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualWishList.Models
{
    class User
    {
        private String name;
        private String password;

        public User(String name, String password)
        {
            this.name = name;
            this.password = password;
        }

        public String getName()
        {
            return name;
        }

        public String getPassword()
        {
            return password;
        }
    }
}
