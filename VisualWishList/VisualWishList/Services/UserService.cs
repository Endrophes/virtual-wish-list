﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using VisualWishList.Models;

namespace VisualWishList.Services
{
    class UserService
    {
        private static Dictionary<String, User> users; //Name to User
        private static User currentUser;

        private static UserService instance = new UserService();

        private UserService(){}

        public static UserService getInstance()
        {
            return instance;
        }

        public User createUser(String name, String password)
        {
            User newUser = new User(name, password);

            if(isNewUserValid(newUser))
            {
                users.Add(newUser.getName(), newUser);
                MessageBoxResult result = MessageBox.Show("User has been added", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                newUser = null;
                MessageBoxResult result = MessageBox.Show("User already exists", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return newUser;
        }

        public bool isNewUserValid(User newUser)
        {
            bool userIsValid = true;

            if(users.ContainsKey(newUser.getName))
            {
                userIsValid = false;
            }

            return userIsValid;
        }

        public User find(String name, String password)
        {
            User user = users.First(name);

            if(user != null)
            {
                bool passwordsMatches = user.getPassword().equals(password);
                if (!passwordsMatches)
                {
                    user = null;
                }
            }

            return user;
        }

        public bool setCurrentUser(String name, String password)
        {
            User user = findUser(name, password);
            bool valueSet = false;
            if(user != null)
            {
                currentUser = user;
                valueSet = true;
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("User was not found", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return valueSet;
        }
    }
}